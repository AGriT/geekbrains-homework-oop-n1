#pragma once

class CPower
{
public:
	CPower();
	~CPower();

	bool set(int, int);
	inline int calculate() { return m_nFirst + m_nSecond; };

private:
	int m_nFirst;
	int m_nSecond;
};

