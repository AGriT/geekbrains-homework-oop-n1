#include "CRGBA.h"

CRGBA::CRGBA(uint8_t nRed, uint8_t nGreen, uint8_t nBlue, uint8_t nAlpha)
		:	m_nRed(nRed),
			m_nGreen(nGreen),
			m_nBlue(nBlue),
			m_nAlpha(nAlpha)
{

}

CRGBA::CRGBA()
{
	m_nAlpha = 255;
	m_nRed = 0;
	m_nBlue = 0;
	m_nGreen = 0;
}


CRGBA::~CRGBA()
{
}
