#include "CStack.h"



CStack::CStack()
{
}


CStack::~CStack()
{
}

void CStack::vReset()
{
	m_nStackSize = 0;

	for (int i = 0; i < sc_nSize; i++)
		m_nStack[i] = 0;

	return;
}

bool CStack::blPush(int nData)
{
	if (m_nStackSize < sc_nSize)
		m_nStack[m_nStackSize++] = nData;
	else
		return false;

	return true;
}

int CStack::nPop()
{
	if (m_nStackSize <= 0)
	{
		std::cout << "Warning! Stack is empty. \n";
		return -1;
	}

	return m_nStack[--m_nStackSize];
}

void CStack::vPrint()
{
	std::cout << "( ";

	for (int i = 0; i < m_nStackSize; i++)
	{
		std::cout << m_nStack[i] << " ";
	}

	std::cout << ")\n";

	return;
}