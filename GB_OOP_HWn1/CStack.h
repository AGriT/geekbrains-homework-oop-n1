#pragma once
#include <iostream>

static const int sc_nSize = 10;

class CStack
{
public:
	CStack();
	~CStack();

	void vReset();
	bool blPush(int);
	int nPop();
	void vPrint();

private:
	int m_nStack[sc_nSize];
	int m_nStackSize;

};

