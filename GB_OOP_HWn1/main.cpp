#include <iostream>
#include "CPower.h"
#include "CStack.h"
#include "CRGBA.h"

void vPrint(CRGBA* pclRGB)
{
	cout << "Red: " << unsigned(pclRGB->m_nRed) << endl;
	cout << "Green: " << unsigned(pclRGB->m_nGreen) << endl;
	cout << "Blue: " << unsigned(pclRGB->m_nBlue) << endl;
	cout << "Alpha: " << unsigned(pclRGB->m_nAlpha) << endl;

	return;
}

int main()
{
	int nCalculated = 0;
	CPower* clPwr = new CPower();

	clPwr->set(2, 3);
	nCalculated = clPwr->calculate();

	std::cout << nCalculated << endl;

	CRGBA* pclRGBSet = new CRGBA(255, 0, 0, 255);
	CRGBA* pclRGBDef = new CRGBA();

	vPrint(pclRGBSet);
	vPrint(pclRGBDef);

	CStack* stack = new CStack();

	stack->vReset();
	stack->vPrint();

	stack->blPush(3);
	stack->blPush(7);
	stack->blPush(5);
	stack->vPrint();

	stack->nPop();
	stack->vPrint();

	stack->nPop();
	stack->nPop();
	stack->vPrint();


	return 0;
}