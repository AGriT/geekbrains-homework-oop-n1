#pragma once
#include <cstdint>
using namespace std;

class CRGBA
{
public:
	CRGBA(uint8_t, uint8_t, uint8_t, uint8_t);
	CRGBA();
	~CRGBA();

	uint8_t m_nRed;
	uint8_t m_nBlue;
	uint8_t m_nGreen;
	uint8_t m_nAlpha;
};

