#include "CPower.h"



CPower::CPower()
{
	m_nFirst = 0;
	m_nSecond = 0;
}


CPower::~CPower()
{
}

bool CPower::set(int nFirst, int nSecond)
{
	m_nFirst = nFirst;
	m_nSecond = nSecond;

	if ((m_nFirst != nFirst) || (m_nSecond != nSecond))
		return false;

	return true;
}